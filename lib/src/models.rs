use eyre::{eyre, Result};
use gluesql::prelude::{Glue, Payload, Value};
use gluesql::sled_storage::sled::IVec;
use gluesql::sled_storage::SledStorage;
use rayon::prelude::*;
use serde::{Deserialize, Serialize};
use std::{fs, path};
use tabled::{Style, Table, Tabled};
use tracing::{error, info};

// Task Struct to manage DB Data
#[derive(Tabled, Serialize, Deserialize, Debug)]
pub struct Task {
    pub id: usize,
    pub task: String,
    pub status: String,
}

/// Creates the connection to a `SledStorage` with gluesql.
/// Panics if `SledStorage` is not preset at path.
/// `GlueSQL` storage configurer
fn glued_storage(path: &str) -> Glue<IVec, SledStorage> {
    let storage = SledStorage::new(path).expect("Something went wrong!");
    Glue::new(storage)
}

// Struct to manage DB paths
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct DB {
    pub path: String,
}

impl DB {
    /// Deletes a task.
    /// Returns a Result<Task> with status "Deleted"
    /// Task is removed from DB
    pub async fn delete(&self, id: usize) -> Result<Task> {
        let task = self.get_task_by_id(id).await?.task;
        let mut glue = glued_storage(&self.path);
        let query = format!("DELETE FROM tasks where id={}", id);
        if glue.execute_async::<_>(&query).await.is_ok() {
            info!("Executing query: {}", &query);
            Ok(Task {
                id,
                task,
                status: "Deleted".to_string(),
            })
        } else {
            error!("Failed to execute query: {}", &query);
            Err(eyre!("Error running query: {}", &query))
        }
    }

    /// Delete Database
    pub async fn delete_db(self) -> Result<Self> {
        if path::Path::new(&self.path).exists() {
            fs::remove_dir_all(&self.path)?;
        }
        Ok(self)
    }

    /// Sets a task's status to "Done".
    /// Returns Task with status "Done"
    pub async fn done(&self, id: usize) -> Result<Task> {
        let task = self.get_task_by_id(id).await?.task;
        let mut glue = glued_storage(&self.path);
        let query = format!("UPDATE tasks SET status=true where id={}", id);
        if glue.execute_async(&query).await.is_ok() {
            info!("Executing query: {}", &query);
            Ok(Task {
                id,
                task,
                status: "Done".to_string(),
            })
        } else {
            error!("Failed to execute query: {}", &query);
            Err(eyre!("Error running query: {}", &query))
        }
    }

    /// Returns the formatted list of this `Tasks`.
    /// This function will return an error if list function
    /// doesn't return a list of `Tasks`.
    pub async fn formatted_list(self) -> Result<String> {
        let table = Table::new(match self.list().await {
            Ok(list) => list,
            Err(err) => {
                error!("Could not retrieve task list: {:?}", err);
                unreachable!("Could not retrieve task list: {:?}", err);
            }
        })
        .with(Style::modern())
        .to_string();
        eprintln!("{}", table);
        Ok(table)
    }

    /// Gets a task by ID, returns a string form of [Task].
    pub async fn get_task_by_id(&self, id: usize) -> Result<Task> {
        let mut glue = glued_storage(&self.path);
        let query = format!("SELECT name, status from tasks where id={}", id);
        let payload_task = if let Ok(payload) = glue.execute_async(&query).await {
            info!("Executing query: {}", &query);
            Ok(payload)
        } else {
            error!("Failed to execute query: {}", &query);
            Err(eyre!("Failed to execute query: {}", &query))
        };
        let rows = match payload_task? {
            Payload::Select { rows, .. } => rows,
            a => {
                error!("Unexpected type: {:?}", a);
                unreachable!("Unexpected type: {:?}", a)
            }
        };
        let task = match &rows[0][0] {
            Value::Str(value) => value.clone(),
            a => {
                error!("Unexpected type: {:?}", a);
                unreachable!("Unexpected type: {:?}", a)
            }
        };
        let status: String = match &rows[0][1] {
            Value::Bool(true) => "Done".to_string(),
            Value::Bool(false) => String::new(),
            a => {
                error!("Unexpected type: {:?}", a);
                unreachable!("Unexpected type: {:?}", a)
            }
        };
        Ok(Task { id, task, status })
    }

    /// Add new task
    pub async fn insert(&self, task: String) -> Result<Task> {
        let last_id: usize = &self.last_available_id().await? + 1;
        let mut glue = glued_storage(&self.path);
        let query = format!(
            "INSERT INTO tasks VALUES ({}, \"{}\", false)",
            last_id, task
        );
        if glue.execute_async(&query).await.is_ok() {
            info!("Executing query: {}", &query);
        } else {
            error!("Failed to execute query: {}", &query);
        };
        Ok(Task {
            id: last_id,
            task,
            status: String::new(),
        })
    }

    /// Get Last available ID
    pub async fn last_available_id(&self) -> Result<usize> {
        let mut glue = glued_storage(&self.path);
        let query = "SELECT MAX(id) FROM tasks where id != NULL";
        let payloads = if let Ok(payload) = glue.execute_async(&query).await {
            info!("Executing query: {}", &query);
            payload
        } else {
            error!("Failed to execute query: {}", &query);
            unreachable!("Failed to execute query: {}", &query)
        };

        let rows = match &payloads {
            Payload::Select { rows, .. } => rows,
            _ => {
                error!("Unexpected result: {:?}", payloads);
                unreachable!("Unexpected type: {:?}", payloads)
            }
        };
        if rows.is_empty() {
            return Ok(0_usize);
        }
        if let &Value::I64(value) = &rows[0][0] {
            Ok(value as usize)
        } else {
            Err(eyre!("Could not retrieve last available id."))
        }
    }

    async fn list(self) -> Result<Vec<Task>> {
        // Create a vector of tasks
        let mut glue = glued_storage(&self.path);
        let query = "SELECT * FROM tasks";
        let payloads = if let Ok(result) = glue.execute_async(query).await {
            info!("Executing query: {}", query);
            Ok(result)
        } else {
            self.new().await.unwrap();
            info!("DB was not created.. Recreating");
            Err(eyre!("DB was not created.. Recreating"))
        };
        let rows = if let Payload::Select { rows, .. } = payloads? {
            Ok(rows)
        } else {
            error!("Unexpected result: Payload unavailable");
            Err(eyre!("Unexpected result: Payload unavailable"))
        };

        let mut tasks = vec![];

        rows?
            .par_iter()
            .map(|row| {
                let id: usize = match &row[0] {
                    Value::I64(value) => *value as usize,
                    a => {
                        error!("Unexpected type: {:?}", a);
                        unreachable!("Unexpected type: {:?}", a)
                    }
                };
                let task = match &row[1] {
                    Value::Str(value) => value.clone(),
                    a => {
                        error!("Unexpected type: {:?}", a);
                        unreachable!("Unexpected type: {:?}", a)
                    }
                };
                let status = match &row[2] {
                    Value::Bool(true) => "Done".to_string(),
                    Value::Bool(false) => String::new(),
                    a => {
                        error!("Unexpected type: {:?}", a);
                        unreachable!("Unexpected type: {:?}", a)
                    }
                };
                Task { id, task, status }
            })
            .collect_into_vec(&mut tasks);

        Ok(tasks)
    }

    /// Remove old data
    /// Create a connection
    /// Setup Tables
    /// Return info or error logging for query executrion
    pub async fn new(self) -> Result<Self> {
        if path::Path::new(&self.path).exists() {
            fs::remove_dir_all(&self.path)?;
        }
        let mut glue = glued_storage(&self.path);
        let queries = "Create Table tasks (id INTEGER, name TEXT, status BOOLEAN)";
        if glue.execute_async(&queries).await.is_ok() {
            info!("Executing query: {}", &queries);
            Ok(Self { path: self.path })
        } else {
            error!("Failed to execute query: {}", &queries);
            Err(eyre!("Failed to execute query: {}", &queries))
        }
    }

    /// Set task to done
    pub async fn undone(&self, id: usize) -> Result<Task> {
        let task = self.get_task_by_id(id).await?.task;
        let mut glue = glued_storage(&self.path);
        let query: String = format!("UPDATE tasks SET status=false where id={}", id);
        if glue.execute_async(&query).await.is_ok() {
            info!("Executing query: {}", &query);

            Ok(Task {
                id,
                task,
                status: "Deleted".to_string(),
            })
        } else {
            error!("Failed to execute query: {}", &query);
            Err(eyre!("Failed to execute query: {}", &query))
        }
    }
}
