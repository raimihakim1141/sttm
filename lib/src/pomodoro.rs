use pbr::ProgressBar;
use std::thread;
use std::time::Duration;

// Return a timer in the form of a loading progress bar
pub fn timer(seconds: usize, statement: &str) {
    let mut pb = ProgressBar::new(seconds as u64);
    pb.format("╢▌▌░╟");
    // set message
    pb.message(statement);
    // disable counter
    pb.show_counter = false;
    // disable speed
    pb.show_speed = false;
    // disable tick
    pb.show_tick = false;
    // disable percentage
    pb.show_percent = false;
    (0..seconds).for_each(|_x| {
        pb.inc();
        thread::sleep(Duration::from_secs(1));
    });
}
