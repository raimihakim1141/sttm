#![cfg_attr(debug_assertions, allow(dead_code, unused_imports))]
use eyre::Result;
use std::time::Duration;
use std::{fs, io, path, thread};
use todo_glue_lib::models::DB;
use tokio_test::block_on;

static PATH: &str = "/tmp/gluesql/test_todo_lib/";

fn get_db() -> DB {
    DB {
        path: PATH.to_string(),
    }
}

#[test]
fn a_db_can_be_created() -> Result<()> {
    let db = get_db();
    let new_db = block_on(db.new())?;
    assert_eq!(PATH, new_db.path);
    Ok(())
}

#[test]
fn b_task_can_be_added() -> Result<()> {
    thread::sleep(Duration::from_secs(3));
    let db = get_db();
    block_on(db.insert("This is a test task".to_string()))?;
    assert_eq!(block_on(db.last_available_id())?, 1);
    block_on(db.formatted_list())?;
    Ok(())
}

#[test]
fn c_add_another_task() -> Result<()> {
    thread::sleep(Duration::from_secs(6));
    let db = get_db();
    block_on(db.insert("This is another test task".to_string()))?;
    assert_eq!(block_on(db.last_available_id())?, 2);
    block_on(db.formatted_list())?;
    Ok(())
}

#[test]
fn d_set_task_done() -> Result<()> {
    thread::sleep(Duration::from_secs(9));
    let db = get_db();
    block_on(db.done(1))?;
    assert_eq!(block_on(db.get_task_by_id(1))?.status, "Done".to_string());
    block_on(db.formatted_list())?;
    Ok(())
}

#[test]
fn f_set_task_undone() -> Result<()> {
    thread::sleep(Duration::from_secs(12));
    let db = get_db();
    block_on(db.undone(1))?;
    assert_eq!(block_on(db.get_task_by_id(1))?.status, String::new());
    block_on(db.formatted_list())?;
    Ok(())
}

#[test]
fn g_task_can_be_deleted() -> Result<()> {
    thread::sleep(Duration::from_secs(15));
    let db = get_db();
    block_on(db.delete(1))?;
    assert_eq!(block_on(db.last_available_id())?, 2);
    block_on(db.formatted_list())?;
    Ok(())
}

#[test]
fn h_task_delete_db() -> Result<()> {
    thread::sleep(Duration::from_secs(18));
    fs::remove_dir_all(PATH)?;
    Ok(())
}

#[test]
fn i_struct_test() {
    let db = DB {
        path: "/tmp/test".to_string(),
    };
    assert_eq!(db.path, "/tmp/test".to_string());
}
