all: test build install

build:
	cargo build --release

install:
	install -v target/release/todo ${HOME}/.local/bin/

test:
	RUST_LOG=info cargo test --all -- --nocapture

lint:
	cargo clippy -- -W clippy::pedantic -W clippy::nursery -W clippy::unwrap_used -W clippy::expect_used --no-deps

fix:
	cargo clippy --fix --allow-dirty -- -W clippy::pedantic -W clippy::nursery -W clippy::unwrap_used -W clippy::expect_used

coverity:
	cargo llvm-cov --all-targets --html --open

commit:
	git cip
	git pull -S origin master
	git push

cd: coverity commit