#![cfg_attr(debug_assertions, allow(dead_code, unused_imports))]
use eyre::Result;
use futures::executor::block_on;
use std::process::{Command, ExitStatus};
use std::time::Duration;
use std::{env, fs, path, thread};
use todo_glue_lib::models::DB;

static PATH: &str = "/tmp/gluesql/data_todo";

fn get_db() -> DB {
    let path = "/tmp/gluesql/test_todo_cli";
    std::env::set_var("TODO_DB_PATH", path);
    DB {
        path: path.to_string(),
    }
}

#[test]
fn a_build_cli() -> Result<()> {
    let _db = get_db();
    let mut cargo: Command = Command::new("cargo");
    let status = cargo.args(["build"]).status()?;
    let status_num: usize = if let Some(code) = status.code() {
        code.try_into()?
    } else {
        1_usize
    };
    assert_eq!(0, status_num);
    Ok(())
}

#[test]
fn b_check_todo_availability() {
    let path_exist = path::Path::new("../target/debug/todo").exists();
    assert!(path_exist);
}

#[test]
fn c_todo_list() -> Result<()> {
    let mut toa: Command = Command::new("../target/debug/todo");
    let status = toa.args(["list"]).status()?;
    let status_num: usize = if let Some(code) = status.code() {
        code.try_into()?
    } else {
        1_usize
    };
    assert_eq!(0, status_num);
    Ok(())
}

#[test]
fn c_todo_list_2() -> Result<()> {
    thread::sleep(Duration::from_secs(2));
    let mut toa: Command = Command::new("../target/debug/todo");
    let status = toa.status()?;
    let status_num: usize = if let Some(code) = status.code() {
        code.try_into()?
    } else {
        1_usize
    };
    assert_eq!(0, status_num);
    Ok(())
}

#[test]
fn d_todo_add() -> Result<()> {
    let db = get_db();
    thread::sleep(Duration::from_secs(4));
    let mut toa: Command = Command::new("../target/debug/todo");
    let status = toa.args(["add", "First", "Test"]).status()?;
    let status_num: usize = if let Some(code) = status.code() {
        code.try_into()?
    } else {
        1_usize
    };
    assert_eq!(block_on(db.last_available_id())?, 1);
    assert_eq!(0, status_num);
    block_on(db.formatted_list())?;
    Ok(())
}

#[test]
fn e_todo_done() -> Result<()> {
    let db = get_db();
    thread::sleep(Duration::from_secs(7));
    let mut toa: Command = Command::new("../target/debug/todo");
    let status = toa.args(["done", "1"]).status()?;
    let status_num: usize = if let Some(code) = status.code() {
        code.try_into()?
    } else {
        1_usize
    };
    assert_eq!(block_on(db.last_available_id())?, 1);
    assert_eq!(block_on(db.get_task_by_id(1))?.status, "Done".to_string());
    assert_eq!(0, status_num);
    block_on(db.formatted_list())?;
    Ok(())
}

#[test]
fn f_todo_undone() -> Result<()> {
    let db = get_db();
    thread::sleep(Duration::from_secs(9));
    let mut toa: Command = Command::new("../target/debug/todo");
    let status = toa.args(["undone", "1"]).status()?;
    let status_num: usize = if let Some(code) = status.code() {
        code.try_into()?
    } else {
        1_usize
    };
    assert_eq!(block_on(db.last_available_id())?, 1);
    assert_eq!(block_on(db.get_task_by_id(1))?.status, String::new());
    assert_eq!(0, status_num);
    block_on(db.formatted_list())?;
    Ok(())
}

#[test]
fn g_todo_del() -> Result<()> {
    let db = get_db();
    thread::sleep(Duration::from_secs(10));
    let mut toa: Command = Command::new("../target/debug/todo");
    let status = toa.args(["del", "1"]).status()?;
    let status_num: usize = if let Some(code) = status.code() {
        code.try_into()?
    } else {
        1_usize
    };
    assert_eq!(block_on(db.last_available_id())?, 0);
    assert_eq!(0, status_num);
    block_on(db.formatted_list())?;
    Ok(())
}

#[test]
fn h_todo_del_db() -> Result<()> {
    thread::sleep(Duration::from_secs(14));
    let mut toa: Command = Command::new("../target/debug/todo");
    let status = toa.args(["start"]).status()?;
    let status_num: usize = if let Some(code) = status.code() {
        code.try_into()?
    } else {
        1_usize
    };
    assert_eq!(0, status_num);
    Ok(())
}

#[test]
fn i_pomo() -> Result<()> {
    thread::sleep(Duration::from_secs(14));
    let mut toa: Command = Command::new("../target/debug/todo");
    let mut status = toa.args(["pomo"]).spawn()?;
    assert_ne!(0, status.id());
    status.kill()?;
    Ok(())
}
