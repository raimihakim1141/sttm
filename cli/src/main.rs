pub mod applet;
pub mod cli_config;
use cli_config::cli;
use eyre::Result;
use futures::executor::block_on;
use todo_glue_lib::{models::DB, pomodoro};
use tracing::error;

///```
///assert_eq!(path(), "/tmp/gluesql/data_todo".to_string());
///std::env::set_var("TODO_DB_PATH", "/tmp/test".to_string());
///assert_eq!(path(), "/tmp/test".to_string());
///std::env::remove_var("TODO_DB_PATH");
///assert_eq!(path(), "/tmp/gluesql/data_todo".to_string());
///```

fn path() -> String {
    if let Ok(output) = std::env::var("TODO_DB_PATH") {
        output
    } else {
        "/tmp/gluesql/data_todo".to_string()
    }
}

fn main() -> Result<()> {
    let db: DB = DB { path: path() };
    let matches = cli().get_matches();
    match matches.subcommand() {
        Some(("start", _sub_matches)) => {
            eprintln!("Creating/Reseting database {}", &db.path);
            block_on(db.new())?;
            Ok(())
        }
        Some(("list", _sub_matches)) => {
            block_on(db.formatted_list())?;
            Ok(())
        }
        Some(("add", sub_matches)) => {
            let task = match sub_matches.get_many::<String>("TASK") {
                Some(data) => data
                    .map(std::string::String::as_str)
                    .collect::<Vec<_>>()
                    .join(" "),
                None => {
                    error!("No taskname given");
                    "No taskname given".to_string()
                }
            };
            eprintln!("adding {}", &task);
            block_on(db.insert(task.trim().to_string()))?;
            block_on(db.formatted_list())?;
            Ok(())
        }
        Some(("del", sub_matches)) => {
            let id = match sub_matches.get_one::<String>("ID") {
                Some(data) => data.as_str(),
                None => {
                    error!("ID unavailable");
                    "No ID given"
                }
            };
            let id = id.parse::<usize>()?;
            let task = block_on(db.get_task_by_id(id))?;
            eprintln!("deleting task {}", task.task);
            block_on(db.delete(task.id))?;
            block_on(db.formatted_list())?;
            Ok(())
        }
        Some(("pomo", _sub_matches)) => {
            block_on(db.formatted_list())?;
            loop {
                pomodoro::timer(25 * 60, "WORK ");
                pomodoro::timer(10 * 60, "REST ");
            }
        }
        Some(("done", sub_matches)) => {
            let id = match sub_matches.get_one::<String>("ID") {
                Some(data) => data.as_str(),
                None => {
                    error!("ID unavailable");
                    "No ID given"
                }
            };
            let id = id.parse::<usize>()?;
            let task = block_on(db.get_task_by_id(id))?;
            eprintln!("Set task {} to done", task.task);
            block_on(db.done(task.id))?;
            block_on(db.formatted_list())?;
            Ok(())
        }
        Some(("undone", sub_matches)) => {
            let id = match sub_matches.get_one::<String>("ID") {
                Some(data) => data.as_str(),
                None => {
                    error!("ID unavailable");
                    "No ID given"
                }
            };
            let id = id.parse::<usize>()?;
            let task = block_on(db.get_task_by_id(id))?;
            eprintln!("Set task {} to undone", task.task);
            block_on(db.undone(task.id))?;
            block_on(db.formatted_list())?;
            Ok(())
        }

        _ => {
            block_on(db.formatted_list())?;
            Ok(())
        }
    }
}

#[cfg(test)]
#[test]
fn test_path() {
    assert_eq!(path(), "/tmp/gluesql/data_todo".to_string());
    std::env::set_var("TODO_DB_PATH", "/tmp/test");
    assert_eq!(path(), "/tmp/test".to_string());
    std::env::remove_var("TODO_DB_PATH");
    assert_eq!(path(), "/tmp/gluesql/data_todo".to_string());
}
