use clap::{arg, Command};

#[must_use]
pub fn start() -> Command<'static> {
    Command::new("start").about("Start/Reset a todo list")
}

#[must_use]
pub fn list() -> Command<'static> {
    Command::new("list").about("List available tasks")
}

#[must_use]
pub fn add() -> Command<'static> {
    Command::new("add")
        .about("Add task to list")
        .trailing_var_arg(true)
        .arg(arg!(<TASK> ... "Task to add"))
        .arg_required_else_help(true)
}

#[must_use]
pub fn del() -> Command<'static> {
    Command::new("del")
        .about("Remove task from list")
        .arg(arg!(<ID> "ID of task to remove"))
        .arg_required_else_help(true)
}

#[must_use]
pub fn pomo() -> Command<'static> {
    Command::new("pomo").about("Start Pomodoro timer")
}

#[must_use]
pub fn done() -> Command<'static> {
    Command::new("done")
        .about("Set task to done")
        .arg(arg!(<ID> "ID of task to set to done"))
        .arg_required_else_help(true)
}

#[must_use]
pub fn undone() -> Command<'static> {
    Command::new("undone")
        .about("Set task to undone")
        .arg(arg!(<ID> "ID of task to set to undone"))
        .arg_required_else_help(true)
}
