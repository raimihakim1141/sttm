use clap::{crate_authors, crate_description, crate_version, Command};

use crate::applet;

#[must_use]
pub fn cli() -> Command<'static> {
    Command::new("todo")
        .about(crate_description!())
        .author(crate_authors!("\n"))
        .version(crate_version!())
        .allow_external_subcommands(false)
        .subcommand(applet::start())
        .subcommand(applet::list())
        .subcommand(applet::add())
        .subcommand(applet::del())
        .subcommand(applet::pomo())
        .subcommand(applet::done())
        .subcommand(applet::undone())
}
