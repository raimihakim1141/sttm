image: docker.io/library/rust:alpine

stages:
  - test-unit
  - test-coverage
  - build

variables:
  CARGO_HOME: /usr/local/cargo
  TZ: UTC

cache: &global_cache
  key: caches
  paths:
    - target/
    - system/

.basescript: &basescript
  - |
    if [ -f "system/apk/alpine-sdk*.apk" ] ; then
      cp -unvr system/apk/* /var/cache/apk/*
      apk add system/apk/*.apk
    else
      apk update
      apk fetch -R -o /var/cache/apk/ alpine-sdk alpine-conf
      apk add /var/cache/apk/*.apk;
      mkdir -pv system/apk/
      cp -r /var/cache/apk/* system/apk/
    fi
  - mkdir -vp system/cargo system/apk
  - |
    if [ -d "system/cargo/bin" ]; then
      cp -uvnr system/cargo/* /usr/local/cargo/
    fi
  - rustup default nightly
  - rustup component add clippy

test-info:
  allow_failure: true
  stage: test-unit
  script:
    - *basescript
    - cargo clippy
    - RUST_LOG=info cargo test --all -- --nocapture
    - cp -uvnr /usr/local/cargo/* system/cargo/
  cache:
     <<: *global_cache
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      when: manual
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
      when: always
      allow_failure: true
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      when: manual
      allow_failure: false

test-coverage:
  allow_failure: true
  stage: test-coverage
  script:
    - *basescript
    - rustup component add llvm-tools-preview
    - cargo install cargo-llvm-cov
    - RUST_LOG=info cargo llvm-cov --workspace --all-targets --html
    - cp -uvnr /usr/local/cargo/* system/cargo/
  cache:
     <<: *global_cache
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      when: always
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      when: always
      allow_failure: false
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
      when: manual
      allow_failure: true

build:
  stage: build
  script:
    - *basescript
    - cargo build ${BUILD_VAR}
    - cp -uvnr /usr/local/cargo/* system/cargo/
  cache:
    <<: *global_cache
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      when: always
      variables:
        BUILD_VAR: --release
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      when: always
      variables:
        BUILD_VAR: --release
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
      when: manual
